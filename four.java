import java.util.Iterator;
import java.util.LinkedList;

public class four {
    public static void main(String[] args) {
        LinkedList<String> ls = new LinkedList<String>();
        ls.add("ertyu");
        ls.add("mknjhgtr");
        ls.add("lmnbvcf");
        ls.add("qwert");
        ls.add("xcbbn");
        // descendingIterator returns last position
        Iterator it = ls.descendingIterator();
        while (it.hasNext()) {
            it.next();
            System.out.println(it.next());
        }
    }

}

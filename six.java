import java.util.LinkedList;

public class six {
    public static void main(String[] args) {
        LinkedList<String> ls = new LinkedList<>();
        ls.add("ertyu");
        ls.add("mknjhgtr");
        ls.add("lmnbvcf");
        ls.add("qwert");
        ls.add("xcbbn");

        // add new element at First
        ls.addFirst("hII");
        // add new element at Last
        ls.addLast("goooo");
    }
}
